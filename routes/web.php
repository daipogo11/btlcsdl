<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'employee'], function() {
	Route::get('/','EmployeeController@listAll');

	Route::get('create', 'EmployeeController@getCreate')->name('create');
	Route::post('create', 'EmployeeController@postCreate')->name('create');

	Route::get('edit/{id}', 'EmployeeController@getEdit')->name('edit');
	Route::post('edit/{id}', 'EmployeeController@postEdit')->name('edit');

	Route::get('delete/{id}', 'EmployeeController@Delete')->name('delete');
});

Route::group(['prefix' => 'expense-report'], function() {
	Route::get('/','ExpenseReportController@listAll');

	Route::get('create','ExpenseReportController@getCreate');
	Route::post('create','ExpenseReportController@postCreate');

	Route::get('edit/{id}', 'ExpenseReportController@getEdit');
	Route::post('edit/{id}', 'ExpenseReportController@postEdit');

	Route::get('delete/{id}','ExpenseReportController@Delete');
});

Route::group(['prefix' => 'expense-detail'], function() {
	Route::get('/','ExpenseDetailController@listAll');

	Route::get('create','ExpenseDetailController@getCreate');
	Route::post('create','ExpenseDetailController@postCreate');

	Route::get('edit/{id}', 'ExpenseDetailController@getEdit');
	Route::post('edit/{id}', 'ExpenseDetailController@postEdit');

	Route::get('delete/{id}','ExpenseDetailController@Delete');
});
