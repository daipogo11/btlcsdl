<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Expense Reports</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <aside class="main-sidebar">
      <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <li>
            <a href="{!!url('employee')!!}">Employees</a>
          </li>
          <li>
            <a href="{!!url('expense-report')!!}">Expense Reports</a>
          </li>
          <li>
            <a href="{!!url('expense-detail')!!}">Expense Details</a>
          </li>
      </ul>
    </aside>
        <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Expense Reports</h3>
              <div class="add"  style="float: right;">            
                  <a id="add" name="add" href="{!!url('expense-report/create')!!}" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Description</th>
                  <th>Purpose</th>
                  <th>Employee</th>
                  <th>Expense Code</th>
                  <th>Report Date</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Date Paid</th>
                  <th>Advance Amount</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($expense_report as $ex)
                  <tr>
                  <td>{{$ex ->Description}}</td>
                  <td>{{$ex->Purpose}}
                  </td>
                  <td>{{\App\employee::find($ex->Employee)->Name}}</td>
                  <td>{{$ex->ExpenseCode}}</td>
                  <td>{{date( "d/m/Y", strtotime($ex->ReportDate))}}</td>
                  <td>{{date( "d/m/Y", strtotime($ex->StartDate))}}</td>
                  <td>{{date( "d/m/Y", strtotime($ex->EndDate))}}</td>
                  <td>{{date( "d/m/Y", strtotime($ex->DatePaid))}}</td>
                  <td>{{$ex->AdvanceAmount}}</td>
                  <td>
                    <a id="edit" name="edit" class="btn btn-primary" href="expense-report/edit/{{$ex->id}}"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="expense-report/delete/{{$ex->id}}" id="delete" name="delete" class="btn btn-danger" style="float: right;"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr>
                @endforeach
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
