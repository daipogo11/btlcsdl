<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Create Employee</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <aside class="main-sidebar">
      <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <li>
            <a href="{!!url('employee')!!}">Employees</a>
          </li>
          <li>
            <a href="{!!url('expense-report')!!}">Expense Reports</a>
          </li>
          <li>
            <a href="{!!url('expense-detail')!!}">Expense Details</a>
          </li>
      </ul>
    </aside>

        <!-- Main content -->
    <section class="content">
      <form role="form" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="box-header">
                <h3 class="box-title">Create Expense Report</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="des">Description</label>
                  <input type="text" class="form-control" id="des" name="description" placeholder="Description">
                </div>
                <div class="form-group">
                  <label for="purposes">Purpose</label>
                  <input type="text" class="form-control" id="purposes" name="purpose" placeholder="Purpose">
                </div>
                <div class="form-group">
                  <label for="employee">Employee</label>
                  <select class="form-control select2" id="employee" name="employee" style="width: 100%;">
                  @foreach($employee as $em)
                    <option>{{$em->Name}}</option>                    
                  @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="ex-code">Expense Code</label>
                  <input type="text" class="form-control" id="ex-code" name="ex_code" placeholder="Expense Code">
                </div>
                <div class="form-group">
                  <label for="reportDate">Report Date</label>
                  <input type="text" class="form-control" id="reportDate" name="reportDate" placeholder="Report Date">
                </div>
                <div class="form-group">
                  <label for="startDate">Start Date</label>
                  <input type="text" class="form-control" id="startDate" name="startDate" placeholder="Start Date">
                </div>
                <div class="form-group">
                  <label for="endDate">End Date</label>
                  <input type="text" class="form-control" id="endDate" name="endDate" placeholder="End Date">
                </div>
                <div class="form-group">
                  <label for="datePaid">Date Paid</label>
                  <input type="text" class="form-control" id="datePaid" name="datePaid" placeholder="Date Paid">
                </div>
                <div class="form-group">
                  <label for="advanceAmount">Advance Amount</label>
                  <input type="text" class="form-control" id="advanceAmount" name="advanceAmount" placeholder="Advance Amount">
                </div>
              </div>
              <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Add</button>
        </div>
          </form>
      </section>
    </div>
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
