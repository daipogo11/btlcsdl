<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expense_report extends Model
{
    protected $table = "expense_reports";
    public $timestamps = false;
    public function expense_report() {
    	return $this->belongsTo('expense_report', 'ExpenseReport');
    }
}
