<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    protected $table = "employees";
    public $timestamps = false;
    public function expense_report() {
    	return $this->hasOne('expense_report');
    }
}
