<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expense_detail extends Model
{
    protected $table = "expense_details";
    public $timestamps = false;
    public function expense_detail() {
    	return $this->hasMany('expense_detail');
    }
}
