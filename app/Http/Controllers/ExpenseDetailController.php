<?php

namespace App\Http\Controllers;
use App\expense_detail;
use Illuminate\Http\Request;
use App\expense_report;
class ExpenseDetailController extends Controller
{
    public function listAll() {
    	$expense_detail = expense_detail::all();
    	return view('expense_detail.index')->with(['expense_detail'=>$expense_detail]);
    }

    public function getCreate() {
    	$expense_report = expense_report::all();
    	return view('expense_detail.create')->with(['expense_report'=>$expense_report]);
    }

    public function postCreate(Request $request) {
    	$em = new expense_detail;
    	$em->Title = $request->title;
    	$em->Cost = $request->cost;
    	$em->ExpenseDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->expense_date)));;
    	$em->Category = $request->category;
    	$er = expense_report::where('Description',$request->expense_report)->take(1)->get();
    	foreach ($er as $e) {
    		$em->ExpenseReport = $e->id;
    	}
    	$em->save();
    	return redirect('expense-detail');
    }

    public function getEdit($id) {
    	$expense_report = expense_report::all();
    	$expense_detail = expense_detail::find($id);
    	return view('expense_detail.edit')->with(['expense_detail'=>$expense_detail,'expense_report'=>$expense_report]);
    }

    public function postEdit(Request $request,$id) {
    	$em = expense_detail::find($id);
    	$em->Title = $request->title;
    	$em->Cost = $request->cost;
    	$em->ExpenseDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->expense_date)));;
    	$em->Category = $request->category;
    	$er = expense_report::where('Description',$request->expense_report)->take(1)->get();
    	foreach ($er as $e) {
    		$em->ExpenseReport = $e->id;
    	}
    	$em->save();
    	return redirect('expense-detail');
    }

    public function Delete($id) {
    	$expense_detail = expense_detail::find($id);
    	$expense_detail->delete();
    	return redirect('expense-detail');
    }
}
