<?php

namespace App\Http\Controllers;
use App\expense_report;
use App\employee;
use Illuminate\Http\Request;

class ExpenseReportController extends Controller
{
    public function listAll() {
    	$expense_report = expense_report::all();
    	return view('expense_report.index')->with(['expense_report'=>$expense_report]);
    }

    public function getCreate() {
    	$employee = employee::all();
    	return view('expense_report.create')->with(['employee'=>$employee]);
    }

    public function postCreate(Request $request) {
    	$er = new expense_report;
    	$er->Description = $request->description;
    	$employee = employee::where('Name',$request->employee)->take(1)->get();
    	foreach ($employee as $em) {
    		$er->Employee = $em->id;
    	}
    	$er->Purpose = $request->purpose;
    	$er->ExpenseCode = $request->ex_code;
    	$er->StartDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->startDate)));
    	$er->EndDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->endDate)));
    	$er->ReportDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->reportDate)));
    	$er->DatePaid = date('Y-m-d', strtotime(str_replace('/', '-', $request->datePaid)));
    	$er->AdvanceAmount = $request->advanceAmount;
    	$er->save();
    	return redirect('expense-report');
    }

    public function getEdit($id) {
    	$employee = employee::all();
    	$expense_report = expense_report::find($id);
    	return view('expense_report.edit')->with(['expense_report'=>$expense_report,'employee'=>$employee]);
    }

    public function postEdit(Request $request,$id) {
    	$er = expense_report::find($id);
    	$er->Description = $request->description;
    	$employee = employee::where('Name',$request->employee)->take(1)->get();
    	foreach ($employee as $em) {
            if($em->id != $er->Employee)
    		  $er->Employee = $em->id;
    	}
    	$er->Purpose = $request->purpose;
    	$er->ExpenseCode = $request->ex_code;
    	$er->StartDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->startDate)));
    	$er->EndDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->endDate)));
    	$er->ReportDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->reportDate)));
    	$er->DatePaid = date('Y-m-d', strtotime(str_replace('/', '-', $request->datePaid)));
    	$er->AdvanceAmount = $request->advanceAmount;
    	$er->save();
    	return redirect('expense-report');
    }

    public function Delete($id) {
    	$expense_report = expense_report::find($id);
    	$expense_report->delete();
    	return redirect('expense-report');
    }
}
