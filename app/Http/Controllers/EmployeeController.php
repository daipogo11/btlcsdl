<?php

namespace App\Http\Controllers;
use App\employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function listAll() {
    	$employee = employee::all();
    	return view('employee.index')->with(['employee'=>$employee]);
    }

    public function getCreate() {
    	return view('employee.create');
    }

    public function postCreate(Request $request) {
    	$em = new employee;
    	$em->Name = $request->name;
    	$em->Age = $request->age;
    	$em->Department = $request->department;
    	$em->Phone_number = $request->phone;
    	$em->Address = $request->address;
    	$em->Email = $request->mail;
    	$em->save();
    	return redirect('employee');
    }

    public function getEdit($id) {
    	$employee = employee::find($id);
    	return view('employee.edit')->with(['employee'=>$employee]);
    }

    public function postEdit(Request $request,$id) {
    	$em = employee::find($id);
    	$em->Name = $request->name;
    	$em->Age = $request->age;
    	$em->Department = $request->department;
    	$em->Phone_number = $request->phone;
    	$em->Address = $request->address;
    	$em->Email = $request->mail;
    	$em->save();
    	return redirect('employee');
    }

    public function Delete($id) {
    	$employee = employee::find($id);
    	$employee->delete();
    	return redirect('employee');
    }
}
